export const getImagesResponse = {
  imageURI: [
    "https://cdn.iconscout.com/icon/premium/png-256-thumb/software-developer-1-910762.png",
    "https://static.thenounproject.com/png/581057-200.png"
  ],
  statusCode: 200
};

export const postImageResponse = {
  imageURI: "https://static.thenounproject.com/png/968731-200.png",
  statusCode: 200
};
